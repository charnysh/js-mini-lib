# JS mini library

Создать JS мини-библиотеку, реализующую заданный функционал (описан ниже) и примеры использования каждого открытого метода.
Подумать над реализацией открытого/закрытого доступа к функциям/переменным данной библиотеки.

## Открытые функции:
### Языковые:

1)  bool isArray(obj) - является ли объект массивом

2)  bool isBoolean(obj) 

3)  bool isDate(obj)

4)  bool isNumber(obj)

5)  bool isString(obj)

6)  bool isFunction(obj)

7)  bool isUndefined(obj)

8)  bool isNull(obj)

### Работа с массивами


1) void     forEach(arr, function action)

2) newArray where(arr, function predicate)

3) obj      first(arr)

4) obj      last(arr)

5) newArray select(arr, function selector)

6) newArray skip(arr, number)

7) newArray take(arr, number)



## Вспомогательные функции:


1) newFunc singleton(func creator) - возвращает новую функцию, которая вызовет creator при первом обращении, 

   все последующие вызовы будут возвращать кешированный результат.

2) newFunc partial(func original, obj arg1) - возвращает новую функцию с меньшим количество параметров:

   Пример:
   function original(arg1, arg2, arg3) { };
   var newFunction = partial(orignal, 'ARG 1');

   newFunction(arg2, arg3); // данный вызов эквивалентен следующему вызову: original('ARG 1', arg2, arg3);

Цепочки вызовов (доп. задание)
Реализовать возможность создания цепочек: asChain(arr). skip(func).take(func)

## JS правки:
(+) 1) foreach function modifies input array items

(+) 2) each variable declaration should be on separate row

(+) 3) using Array.push method instead of newArray[new_i++] = arr[i]; will simplify your code

(+) 4) don't insert a space before parenthesis in function calling

(+) 5) insert space after comma and the next function parameter

(+) 6) always insert semicolon after instructions

(+) 7) there is no need of "instantiated" variable

(+-) 8) javascript modules

(+) 9) make current_arr private in arrayFunctions 


## Правки 18.07

(+) 1) arrayFunctions select [].push is enough

(+) 2) asChain.forEach returns undefined

(+) 3) chainable skip, take, etc. can use array_functions methods, otherwise it just duplicates existing code

(+) 4) don't reassign function arguments

## JS (20.07)
(-) 1) memoizable function doesn't return cached results (OK in es6 branch)

(+) 2) remove unused variables

(+) 3) asChain first, last functions should call already created functions you have

(+) 4) refactor singleton function (can be written in less amount of lines)

(+) 5) why not to use exiting functions for array (filter, map)?