var wrappers = (function () {
    return {
        singleton: function (creator) {
            var instance;
            return function () {
                if (!instance) {
                    instance = creator.apply(this, arguments);
                }
                return instance;
            };
        },

        partial: function (original) {
            var presetArgs = Array.prototype.slice.call(arguments, 1);
            return function () {
                currentArgs = Array.prototype.slice.call(arguments);
                Array.prototype.unshift.apply(currentArgs, presetArgs);
                return original.apply(this, currentArgs);
            };
        },
    };
}());