var arrayFunctions = (function () {
    var functions = {
        memoizable: function (f) {
            let memo = {};
            return function (p) {
                if (!memo.hasOwnProperty(p)) {
                    let result = f.apply(this, arguments);
                    memo[p] = result;
                    return result;
                }
                console.log("I've seen this before...");
                return memo[p];
            };
        },

        forEach: function (arr, action) {
            for (var i = 0; i < arr.length; ++i) {
                action(arr[i]);
            }
        },

        where: function (arr, predicate) {
            var newArray = arr.filter(predicate);
            return newArray;
        },

        first: function (arr) {
            return arr[0];
        },


        last: function (arr) {
            return arr[arr.length - 1];
        },

        select: function (arr, selector) {
            var resultArray = arr.map(selector);
            return resultArray;
        },

        skip: function (arr, number) {
            return arr.slice(number - 1);
        },

        take: function (arr, number) {
            return arr.slice(0, number);
        },
        asChain: function (arr) {
            var currentArr = arr;
            var that = this;
            var chainable = {
                forEach: function (action) {
                    that.forEach(currentArr, action);
                    return this;
                },

                where: function (predicate) {
                    currentArr = that.where(currentArr, predicate);
                    return this;
                },

                first: function () {
                    return currentArr[0];
                },

                last: function () {
                    return currentArr[currentArr.length - 1];
                },

                select: function (selector) {
                    currentArr = that.select(currentArr, selector);
                    return this;
                },

                skip: function (number) {
                    currentArr = that.skip(currentArr, number);
                    return this;
                },

                take: function (number) {
                    currentArr = that.take(currentArr, number);
                    return this;
                },

                return: function () {
                    return currentArr;
                },
            };
            return chainable;
        },
    };
    return functions;
})();